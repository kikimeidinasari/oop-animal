<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    $animal = new animal("Shaun");
    echo "Name : ". $animal->name ."<br>";
    echo "Legs : ". $animal->legs ."<br>";
    echo "Cold Blooded : ". $animal->cold_blooded ."<br><br>";

    $ape = new ape("Buduk");
    echo "Name : ". $ape->name ."<br>";
    echo "Legs : ". $ape->legs ."<br>";
    echo "Cold Blooded : ". $ape->cold_blooded ."<br>";
    echo "Jump : ";
    echo $ape->jump()."<br><br>";

    $frog = new frog("Kera Sakti");
    echo "Name : ". $frog->name ."<br>";
    echo "Legs : ". $frog->legs ."<br>";
    echo "Cold Blooded : ". $frog->cold_blooded ."<br>";
    echo "Yell : ";
    echo $frog->yell() ;

?>